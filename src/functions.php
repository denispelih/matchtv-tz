<?php

namespace App;


function get_parse_str($str, $strict = true) {
    $parsed = [];
    parse_str($str, $parsed);

    $invalid = $strict
        && count($parsed) === 1
        && isset($parsed[$str])
        && empty($parsed[$str]);

    return !$invalid ? $parsed : [];
}


function json_pretty_print_flags() {
    return JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
}