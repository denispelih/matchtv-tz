<?php

namespace AppBundle\Command\Tool;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetDbCommand extends ContainerAwareCommand
{
    /**
     * Current application environment.
     *
     * @var string
     */
    protected $env;

    /**
     * Available environment.
     *
     * @var array
     */
    protected $environmentAvailable = ['test', 'dev'];

    protected $commands = [
        [
            'name'=> 'doctrine:database:drop',
            'arguments' => [
                '--if-exists' => true,
                '--force' => true,
            ]
        ],
        [
            'name' => 'doctrine:database:create',
            'arguments' => [],
        ],
        [
            'name'=> 'doctrine:schema:update',
            'arguments' => [
                '--force' => true,
            ]
        ],
        [
            'name' => 'doctrine:fixtures:load',
            'arguments' => [
                '--append' => true,
            ],
        ],
    ];

    protected function configure()
    {
        $this
            ->setName('app:tool:db:reset')
            ->setDescription('Clean and recreate database and load fixtures');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->env = $this->getContainer()->getParameter('kernel.environment');
        if (!in_array($this->env, $this->environmentAvailable)) {
            throw new \RuntimeException(sprintf(
                'Current environment not supported. Supported: [%s]',
                implode(', ', $this->environmentAvailable)
            ));
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Resetting environment - {$this->env}");

        try {
            $this->getContainer()->get('doctrine.dbal.default_connection')->exec('SET foreign_key_checks = 0;');
        } catch (\Exception $e) {}

        foreach ($this->commands as $cmd) {
            $command = $this->getApplication()->find($cmd['name']);
            $arguments = $cmd['arguments'];
            $arguments['--env'] = $this->env;
            $arguments[] = '-vvv';
            $input = new ArrayInput($arguments);
            $input->setInteractive(false);
            $command->run($input, $output);
        }

        $this->getContainer()->get('doctrine.dbal.default_connection')->exec('SET foreign_key_checks = 1;');
    }
}
