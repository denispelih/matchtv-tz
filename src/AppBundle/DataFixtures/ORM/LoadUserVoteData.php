<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Entity\UserVote;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserVoteData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $this->getReference('maksim.kis');
        $game = $this->getReference('Зенит-ЦСКА');
        $team = $this->getReference('ЦСКА');
        $vote = $this->getReference('Голосование: ' . (string) $game);
        $uv   = (new UserVote())
            ->setUser($user)
            ->setVote($vote)
            ->setVotedTeam($team)
            ->setCreatedAt(new \DateTime('23.02.2017'));
        $manager->persist($uv);

        /** @var User $user */
        $user = $this->getReference('maksim.kis');
        $game = $this->getReference('Спартак-ЦСКА');
        $team = $this->getReference('ЦСКА');
        $vote = $this->getReference('Голосование: ' . (string) $game);
        $uv   = (new UserVote())
            ->setUser($user)
            ->setVote($vote)
            ->setVotedTeam($team)
            ->setCreatedAt(new \DateTime('23.02.2017'));
        $manager->persist($uv);

        /** @var User $user */
        $user   = $this->getReference('maksim.kis');
        $game   = $this->getReference('Спартак-Зенит');
        $player = $this->getReference('Комбаров');
        $vote   = $this->getReference('Голосование: ' . (string) $game);
        $uv     = (new UserVote())
            ->setUser($user)
            ->setVote($vote)
            ->setVotedPlayer($player)
            ->setCreatedAt(new \DateTime('23.02.2017'));
        $manager->persist($uv);

        /** @var User $user */
        $user   = $this->getReference('marat.salimgareev');
        $game   = $this->getReference('Спартак-Зенит');
        $player = $this->getReference('Комбаров');
        $vote   = $this->getReference('Голосование: ' . (string) $game);
        $uv     = (new UserVote())
            ->setUser($user)
            ->setVote($vote)
            ->setVotedPlayer($player)
            ->setCreatedAt(new \DateTime('23.02.2017'));
        $manager->persist($uv);

        /** @var User $user */
        $user   = $this->getReference('denis.pelikh');
        $game   = $this->getReference('Спартак-Зенит');
        $player = $this->getReference('Лодыгин');
        $vote   = $this->getReference('Голосование: ' . (string) $game);
        $uv     = (new UserVote())
            ->setUser($user)
            ->setVote($vote)
            ->setVotedPlayer($player)
            ->setCreatedAt(new \DateTime('23.02.2017'));
        $manager->persist($uv);

        $manager->flush();
    }


    public function getOrder()
    {
        return 6;
    }
}