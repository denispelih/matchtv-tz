<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Game;
use AppBundle\Entity\Score;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGameData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $gamesData = [
            ['Зенит', 'ЦСКА', '10.12.2016 21:00:00', '10.12.2016 22:40:00', 0, 2],
            ['Спартак', 'ЦСКА', '01.01.2017 01:30:00', '01.01.2017 03:14:00', 1, 0],
            ['Спартак', 'Зенит', '06.02.2017 23:45:00', '07.02.2017 01:21:00', 2, 3],
        ];

        foreach ($gamesData as list($ht, $at, $sdt, $edt, $hs, $as)) {
            $game = new Game();
            $game->setHomeTeam($this->getReference($ht));
            $game->setAwayTeam($this->getReference($at));
            $game->setStartDate(new \DateTime(($sdt)));
            $game->setEndDate(new \DateTime(($edt)));
            $game->setScore(new Score($hs, $as));
            $manager->persist($game);
            $this->addReference("{$ht}-{$at}", $game);
        }

        $manager->flush();
    }


    public function getOrder()
    {
        return 4;
    }
}