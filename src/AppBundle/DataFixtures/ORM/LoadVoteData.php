<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Vote;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadVoteData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $votesData = [
            [$this->getReference('Зенит-ЦСКА'), Vote::TYPE_ON_TEAM, Vote::COST_FREE],
            [$this->getReference('Спартак-ЦСКА'), Vote::TYPE_ON_TEAM, Vote::COST_NON_FREE],
            [$this->getReference('Спартак-Зенит'), Vote::TYPE_ON_PLAYER, Vote::COST_NON_FREE],
            [$this->getReference('Зенит-ЦСКА'), Vote::TYPE_ON_PLAYER, Vote::COST_FREE],
        ];

        foreach ($votesData as list($game, $type, $cost)) {
            $vote = new Vote();
            $vote->setGame($game);
            $vote->setType($type);
            $vote->setCost($cost);
            $manager->persist($vote);
            try {
                $this->addReference('Голосование: ' . (string) $game, $vote);
            } catch (\BadMethodCallException $e) {}
        }

        $manager->flush();
    }


    public function getOrder()
    {
        return 5;
    }
}