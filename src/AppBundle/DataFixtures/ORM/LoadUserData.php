<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (['maksim.kis', 'marat.salimgareev', 'denis.pelikh'] as $name) {
            $user = new User();
            $user->setName($name);
            $user->setMoney(5);
            $manager->persist($user);
            $this->addReference($name, $user);
        }

        $user = new User();
        $user->setName('Tester w/o money');
        $manager->persist($user);
        $this->addReference('Tester w/o money', $user);

        $manager->flush();
    }


    public function getOrder()
    {
        return 1;
    }
}