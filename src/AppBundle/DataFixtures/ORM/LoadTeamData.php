<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Team;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTeamData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (['Зенит', 'ЦСКА', 'Спартак'] as $name) {
            $team = new Team();
            $team->setName($name);
            $manager->persist($team);
            $this->addReference($name, $team);
        }

        $manager->flush();
    }


    public function getOrder()
    {
        return 2;
    }
}