<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Player;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlayerData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $playersData = [
            'Акинфеев' => 'ЦСКА',
            'Лодыгин'  => 'Зенит',
            'Комбаров' => 'Спартак',
        ];
        foreach ($playersData as $name => $team) {
            $player = new Player();
            $player->setName($name);
            $player->setTeam($this->getReference($team));
            $manager->persist($player);
            $this->addReference($name, $player);
        }

        $manager->flush();
    }


    public function getOrder()
    {
        return 3;
    }
}