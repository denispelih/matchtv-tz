<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 */
class User implements UserInterface
{
    const DEFAULT_UNSAFE_PASSWORD = 'matchclub';

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"auth_full"})
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"auth_full"})
     */
    private $name;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"auth_full"})
     */
    private $money = 0;


    public function getRoles()
    {
        return ['ROLE_USER'];
    }


    public function getPassword()
    {
        return static::DEFAULT_UNSAFE_PASSWORD;
    }


    public function getSalt()
    {
        return '';
    }


    public function getUsername()
    {
        return $this->getName();
    }


    public function eraseCredentials()
    {
        // nothing
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set money
     *
     * @param integer $money
     *
     * @return User
     */
    public function setMoney($money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * Get money
     *
     * @return integer
     */
    public function getMoney()
    {
        return $this->money;
    }


    public function __toString()
    {
        return $this->getName();
    }
}
