<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 */
class Game
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"auth_all"})
     */
    private $id;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @Serializer\Groups({"auth_all"})
     */
    private $startDate;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @Serializer\Groups({"auth_all"})
     */
    private $endDate;
    /**
     * @var Team
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Team")
     * @ORM\JoinColumn(name="home_team_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Serializer\Groups({"auth_all"})
     */
    private $homeTeam;
    /**
     * @var Team
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Team")
     * @ORM\JoinColumn(name="away_team_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Serializer\Groups({"auth_all"})
     */
    private $awayTeam;
    /**
     * @var Score
     * @ORM\Embedded(class="AppBundle\Entity\Score", columnPrefix="score_")
     * @Assert\NotNull()
     * @Serializer\Groups({"auth_all"})
     */
    private $score;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Game
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Game
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set score
     *
     * @param \AppBundle\Entity\Score $score
     *
     * @return Game
     */
    public function setScore(\AppBundle\Entity\Score $score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return \AppBundle\Entity\Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set homeTeam
     *
     * @param \AppBundle\Entity\Team $homeTeam
     *
     * @return Game
     */
    public function setHomeTeam(\AppBundle\Entity\Team $homeTeam)
    {
        $this->homeTeam = $homeTeam;

        return $this;
    }

    /**
     * Get homeTeam
     *
     * @return \AppBundle\Entity\Team
     */
    public function getHomeTeam()
    {
        return $this->homeTeam;
    }

    /**
     * Set awayTeam
     *
     * @param \AppBundle\Entity\Team $awayTeam
     *
     * @return Game
     */
    public function setAwayTeam(\AppBundle\Entity\Team $awayTeam)
    {
        $this->awayTeam = $awayTeam;

        return $this;
    }

    /**
     * Get awayTeam
     *
     * @return \AppBundle\Entity\Team
     */
    public function getAwayTeam()
    {
        return $this->awayTeam;
    }


    public function __toString()
    {
        return $this->getHomeTeam()->getName().'-'.$this->getAwayTeam()->getName();
    }
}
