<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Embeddable
 */
class Score
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     * @Serializer\Groups({"auth_all"})
     */
    private $homeTeam = 0;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     * @Serializer\Groups({"auth_all"})
     */
    private $awayTeam = 0;


    public function __construct($homeTeam, $awayTeam)
    {
        $this->homeTeam = $homeTeam;
        $this->awayTeam = $awayTeam;
    }

    public function getHomeTeam()
    {
        return $this->homeTeam;
    }

    public function setHomeTeam($homeTeam)
    {
        $this->homeTeam = $homeTeam;
    }

    public function getAwayTeam()
    {
        return $this->awayTeam;
    }

    public function setAwayTeam($awayTeam)
    {
        $this->awayTeam = $awayTeam;
    }
}