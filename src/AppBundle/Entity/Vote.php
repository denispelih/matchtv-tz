<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VoteRepository")
 */
class Vote
{
    const TYPE_ON_PLAYER  = 'on_player';
    const TYPE_ON_TEAM    = 'on_team';

    const COST_FREE     = 0;
    const COST_NON_FREE = 1;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"auth_all"})
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=16, name="`type`")
     * @Assert\Choice(choices={
     *     Vote::TYPE_ON_PLAYER,
     *     Vote::TYPE_ON_TEAM
     * })
     * @Serializer\Groups({"auth_all"})
     */
    private $type = self::TYPE_ON_TEAM;
    /**
     * @var integer
     * @ORM\Column(type="integer", name="`interval`")
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     * @Serializer\Groups({"auth_all"})
     */
    private $interval = 60;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @Assert\Choice(choices={
     *     Vote::COST_FREE,
     *     Vote::COST_NON_FREE
     * })
     * @Serializer\Groups({"auth_all"})
     */
    private $cost = self::COST_FREE;
    /**
     * @var Game
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Serializer\Groups({"auth_all"})
     */
    private $game;
    /**
     * @var UserVote[]|Collection|Selectable
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserVote", mappedBy="vote", fetch="EAGER")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @Serializer\Exclude()
     */
    private $userVotes;


    /** @return bool */
    public function isVoteOnTeam()
    {
        return $this->type === self::TYPE_ON_TEAM;
    }


    /** @return bool */
    public function isVoteOnPlayer()
    {
        return $this->type === self::TYPE_ON_PLAYER;
    }

    /** @return int */
    public function getId()
    {
        return $this->id;
    }


    /** @return Game */
    public function getGame()
    {
        return $this->game;
    }


    /** @return $this */
    public function setGame(Game $game)
    {
        $this->game = $game;
        return $this;
    }


    /** @return string */
    public function getType()
    {
        return $this->type;
    }


    /** @return $this */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    /** @return int */
    public function getInterval()
    {
        return $this->interval;
    }


    /** @return $this */
    public function setInterval($interval)
    {
        $this->interval = $interval;
        return $this;
    }


    /** @return int */
    public function getCost()
    {
        return $this->cost;
    }


    /** @return $this */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }


    /** @return $this */
    public function setUserVotes(Collection $userVotes)
    {
        $this->userVotes = $userVotes;
        return $this;
    }


    /** @return UserVote[]|Collection|Selectable */
    public function getUserVotes(User $user = null)
    {
        if (!$user) {
            return $this->userVotes;
        }

        $c = Criteria::create()
            ->where(Criteria::expr()->eq('user', $user));

        return $this->userVotes->matching($c);
    }


    /** @return UserVote[]|Collection|Selectable */
    public function getUserVotesByTeam(Team $team, User $user = null)
    {
        $c = Criteria::create()
            ->where(Criteria::expr()->eq('votedTeam', $team));

        return $this->getUserVotes($user)->matching($c);
    }
}
