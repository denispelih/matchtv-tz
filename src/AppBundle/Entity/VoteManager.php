<?php

namespace AppBundle\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class VoteManager
{
    /** @var  EntityManager */
    private $em;


    /**
     * @param User $user
     * @param Vote $vote
     * @param int  $voted
     * @return UserVote
     */
    public function toVote(User $user, Vote $vote, $voted)
    {
        return $this->em->transactional(function (EntityManager $em) use($user, $vote, $voted) {
            if ($this->getTimeout($user, $vote) > 0) {
                throw new \LogicException('Timeout greater then 0');
            }

            $userVote = $this->createUserVote($user, $vote);
            $this->resolveUserVote($userVote, $voted);

            $em->refresh($user);
            $user->setMoney($user->getMoney() - $vote->getCost());

            if ($user->getMoney() < 0) {
                throw new \LogicException('Not enough money');
            }

            return $userVote;
        });
    }


    public function getTimeout(User $user, Vote $vote)
    {
        /** @var UserVote $userVote */
        $userVote = $this->getUserVoteRepository()->findOneBy(
            ['user' => $user, 'vote' => $vote],
            ['createdAt' => 'DESC']
        );
        if (!$userVote) {
            return 0;
        }

        $timeout = $vote->getInterval() - (time() - $userVote->getCreatedAt()->getTimestamp());

        return $timeout > 0 ? $timeout : 0;
    }


    public function countUserVotes(User $user, Vote $vote)
    {
        return $vote->getUserVotes($user)->count();
    }


    public function countUserVotesByTeams(User $user, Vote $vote)
    {
        if (!$vote->isVoteOnTeam()) {
            return null;
        }

        $homeTeam = $vote->getGame()->getHomeTeam();
        $awayTeam = $vote->getGame()->getAwayTeam();

        return [
            (string) $homeTeam => $vote->getUserVotesByTeam($homeTeam, $user)->count(),
            (string) $awayTeam => $vote->getUserVotesByTeam($awayTeam, $user)->count(),
        ];
    }


    private function createUserVote(User $user, Vote $vote)
    {
        $userVote = new UserVote();
        $userVote->setVote($vote);
        $userVote->setUser($user);
        $this->em->persist($userVote);

        return $userVote;
    }


    private function resolveUserVote(UserVote $userVote, $votedObjectId)
    {
        $homeTeam = $userVote->getVote()->getGame()->getHomeTeam();
        $awayTeam = $userVote->getVote()->getGame()->getAwayTeam();

        if ($userVote->getVote()->isVoteOnTeam()) {
            switch ($votedObjectId) {
                case $homeTeam->getId():
                    $userVote->setVotedTeam($homeTeam);
                    break;
                case $awayTeam->getId():
                    $userVote->setVotedTeam($awayTeam);
                    break;
                default:
                    throw new \RuntimeException('Invalid votedObjectId');
            }
            return;
        }

        if ($userVote->getVote()->isVoteOnPlayer()) {
            $player = $this->em->getRepository(Player::class)->find($votedObjectId);
            switch ($player->getTeam()->getId()) {
                case $homeTeam->getId():
                case $awayTeam->getId():
                    $userVote->setVotedPlayer($player);
                    break;
                default:
                    throw new \RuntimeException('Invalid votedObjectId');
            }
            return;
        }

        throw new \RuntimeException('Invalid type of vote');
    }


    /** @return VoteRepository */
    public function getRepository()
    {
        return $this->em->getRepository(Vote::class);
    }


    public function getUserVoteRepository()
    {
        return $this->em->getRepository(UserVote::class);
    }


    public function setEntityManager(ObjectManager $em)
    {
        $this->em = $em;
        return $this;
    }
}