<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class VoteRepository extends EntityRepository
{
    /**
     * @param  $offset
     * @param  $limit
     * @return Vote[]|Collection
     */
    public function findVotes($offset, $limit)
    {
        $ids = $this->findVoteIds($offset, $limit);

        if (!$ids) {
            return [];
        }

        $qb = $this->createQueryBuilder('v')
            ->select('v', 'g', 'uv')
            ->join('v.game', 'g')
            ->leftJoin('v.userVotes', 'uv')
            ->where('v.id IN (:ids)')->setParameter('ids', $ids)
            ->orderBy('g.startDate', 'DESC');

        $votes = $qb->getQuery()->getResult(Query::HYDRATE_OBJECT);

        return $votes;
    }


    /**
     * @param $offset
     * @param $limit
     * @return array
     */
    private function findVoteIds($offset, $limit)
    {
        $qb = $this->createQueryBuilder('v')
            ->select('v.id')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $ids = $qb->getQuery()->getScalarResult();

        return $ids;
    }
}