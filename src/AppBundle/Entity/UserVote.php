<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 */
class UserVote
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"auth_full"})
     */
    private $id;
    /**
     * @var Vote
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Vote", inversedBy="userVotes")
     * @ORM\JoinColumn(name="vote_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Serializer\Exclude()
     */
    private $vote;
    /**
     * @var Team
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Team")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @Serializer\Groups({"auth_full"})
     */
    private $votedTeam;
    /**
     * @var Player
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @Serializer\Groups({"auth_full"})
     */
    private $votedPlayer;
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Serializer\Groups({"auth_full"})
     */
    private $user;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @Serializer\Groups({"auth_full"})
     */
    private $createdAt;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UserVote
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set vote
     *
     * @param \AppBundle\Entity\Vote $vote
     *
     * @return UserVote
     */
    public function setVote(\AppBundle\Entity\Vote $vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return \AppBundle\Entity\Vote
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set votedTeam
     *
     * @param \AppBundle\Entity\Team $votedTeam
     *
     * @return UserVote
     */
    public function setVotedTeam(\AppBundle\Entity\Team $votedTeam = null)
    {
        $this->votedTeam = $votedTeam;

        return $this;
    }

    /**
     * Get votedTeam
     *
     * @return \AppBundle\Entity\Team
     */
    public function getVotedTeam()
    {
        return $this->votedTeam;
    }

    /**
     * Set votedPlayer
     *
     * @param \AppBundle\Entity\Player $votedPlayer
     *
     * @return UserVote
     */
    public function setVotedPlayer(\AppBundle\Entity\Player $votedPlayer = null)
    {
        $this->votedPlayer = $votedPlayer;

        return $this;
    }

    /**
     * Get votedPlayer
     *
     * @return \AppBundle\Entity\Player
     */
    public function getVotedPlayer()
    {
        return $this->votedPlayer;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserVote
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    public function __toString()
    {
        return (string) $this->getUser()->getName();
    }
}
