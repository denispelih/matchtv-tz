<?php

namespace AppBundle\Serializer\Subscriber;

use AppBundle\Entity\User;
use AppBundle\Entity\Vote;
use AppBundle\Entity\VoteManager;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\GenericSerializationVisitor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class VoteSubscriber implements EventSubscriberInterface
{
    /** @var  VoteManager */
    private $voteManager;
    /** @var  TokenStorageInterface */
    private $tokenStorage;


    public function __construct(VoteManager $voteManager, TokenStorageInterface $tokenStorage)
    {
        $this->voteManager  = $voteManager;
        $this->tokenStorage = $tokenStorage;
    }


    public static function getSubscribedEvents()
    {
        return [
            [
                'event'  => 'serializer.post_serialize',
                'method' => 'onPostSerialize',
                'class'  => Vote::class
            ]
        ];
    }


    public function onPostSerialize(ObjectEvent $event)
    {
        if (!in_array('auth_full', $event->getContext()->attributes->get('groups')->get(), true)) {
            return;
        }

        /** @var GenericSerializationVisitor $visitor */
        $visitor = $event->getVisitor();
        /** @var Vote $vote */
        $vote    = $event->getObject();
        $user    = $this->getUser();

        $userVotes = $vote->getUserVotes($user);
        $visitor->addData('userVotes', $userVotes);

        $timeout = $this->voteManager->getTimeout($user, $vote);
        $visitor->addData('timeout', $timeout);

        $countUserVotes = $this->voteManager->countUserVotes($user, $vote);
        $visitor->addData('countUserVotes', $countUserVotes);

        $countUserVotesByTeams = $this->voteManager->countUserVotesByTeams($user, $vote);
        $visitor->addData('countUserVotesByTeams', $countUserVotesByTeams);
    }


    /**
     * @return User
     */
    public function getUser()
    {

        if (!$token = $this->tokenStorage->getToken()) {
            throw new \RuntimeException('Not authenticated');
        }

        if (!is_object($user = $token->getUser())) {
            throw new \RuntimeException('Not authenticated');
        }

        return $user;
    }
}