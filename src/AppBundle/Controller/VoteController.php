<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Vote;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

class VoteController extends FOSRestController
{
    /**
     * @Post("/api/votes/{id}/userVotes", requirements={"id" = "\d+"})
     * @RequestParam(name="voted", key="voted", requirements="\d+", strict=true, nullable=false, allowBlank=false)
     * @View(statusCode=201)
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function postVoteAction(Vote $vote, $voted)
    {
        $voteManager = $this->get('app.manager.vote');

        try {
            $userVote = $voteManager->toVote($this->getUser(), $vote, $voted);
        } catch (\LogicException $e) {
            throw $this->createAccessDeniedException($e->getMessage());
        } catch (\RuntimeException $e) {
            throw new HttpException(400, $e->getMessage());
        }

        return $this->toArray($userVote);
    }

    /**
     * @Get("/api/votes")
     * @QueryParam(name="offset", key="offset", requirements="\d+", default="0")
     * @QueryParam(name="limit", key="limit", requirements="\d+", default="10")
     */
    public function getVotesAction($offset, $limit)
    {
        $repository = $this->get('app.manager.vote')->getRepository();
        $votes      = $repository->findVotes($offset, $limit);

        return $this->toArray($votes);
    }

    /**
     * @Get("/api/votes/{id}")
     */
    public function getVoteAction(Vote $vote)
    {
        return $this->toArray($vote);
    }


    private function toArray($data)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $groups = $this->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)
            ? ['auth_full', 'auth_all']
            : ['auth_all'];
        $context = SerializationContext::create()->setGroups($groups);
        $array   = $serializer->toArray($data, $context);

        return $array;
    }
}