Матч! Клуб
=======

Тестовое задание для проекта __Матч! Клуб__. 

## Установка

```sh
$ git clone git@bitbucket.org:denispelih/matchtv-tz.git
$ cd matchtv-tz
$ php ./composer.phar install
$ php ./app/console app:tool:db:reset
$ php ./app/console server:run
```

## Тестирование

Для тестирования используется библиотека [Behat](http://behat.org). 

```sh
$ php ./bin/behat
```

## Контакты

[Денис Пелих](mailto:denis.pelih@gmail.com)