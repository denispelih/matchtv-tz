<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\Environment\InitializedContextEnvironment;
use Behat\Behat\Hook\Scope\BeforeFeatureScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;

class FeatureContext implements Context
{
    /**
     * @BeforeScenario
     */
    public function setupScenario(BeforeScenarioScope $scope)
    {
        /** @var InitializedContextEnvironment $env */
        $env = $scope->getEnvironment();
        /** @var HttpContext $hc */
        $hc  = $env->getContext(HttpContext::class);
        /** @var SymfonyContext $sc */
        $sc  = $env->getContext(SymfonyContext::class);

        $hc->setClient($sc->createHttpClient());
    }

    /**
     * @BeforeFeature
     */
    public static function setupFeature(BeforeFeatureScope $scope)
    {
        $fnConsole = __DIR__.'/../../app/console';
        $resetCommand = sprintf('php -dxdebug.enable=0 -dxdebug.remote_enable=0 %s app:tool:db:reset -vv --env=test', $fnConsole);
        print shell_exec($resetCommand);
    }
}
