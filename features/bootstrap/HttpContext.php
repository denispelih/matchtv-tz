<?php

use Assert\Assertion;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use function GuzzleHttp\Psr7\stream_for;
use function App\json_pretty_print_flags;

class HttpContext implements Context
{
    /** @var  ClientInterface */
    protected $client;

    /** @var  RequestInterface */
    protected $request;
    /** @var  ResponseInterface */
    protected $response;

    /** @var  string */
    protected $baseUri;


    /** @Given base uri is :baseUri for sending http request */
    public function baseUriForSendingHttpRequest($baseUri)
    {
        $this->baseUri = $baseUri;
    }


    /** @Given I want to send http request :method to :uri */
    public function iWantToSendHttpRequestTo($method, $uri)
    {
        $this->request = new Request($method, $uri);
    }


    /** @Given request has http basic auth: */
    public function requestHasHttpBasicAuth(TableNode $table)
    {
        $user     = $table->getRowsHash()['user'];
        $password = $table->getRowsHash()['password'];
        $auth     = 'Basic ' . base64_encode("$user:$password");
        $this->request = $this->request->withHeader('Authorization', $auth);
    }


    /** @Given request has parameters: */
    public function requestHasParameters(TableNode $table)
    {
        $uri = $this->request->getUri() ?: new Uri();
        foreach ($table->getRowsHash() as $key => $value) {
            $uri = Uri::withQueryValue($uri, $key, $value);
        }
        $this->request = $this->request->withUri($uri);
    }


    /** @Given request has body with :contentType: */
    public function requestHasBodyWithText($contentType, PyStringNode $body)
    {
        $this->request = $this->request
            ->withHeader('Content-Type', $contentType)
            ->withHeader('Accept', $contentType)
            ->withBody(stream_for($body));
    }


    /** @When I send it */
    public function iSendIt()
    {
        $this->response = $this->getClient()->send($this->request);
    }


    /** @When I send http request :method to :uri */
    public function iSendHttpRequestTo($method, $uri)
    {
        $this->iWantToSendHttpRequestTo($method, $uri);
        $this->iSendIt();
    }


    /** @When I send http request :method to :uri with parameters: */
    public function iSendHttpRequestToWithParameters($method, $uri, TableNode $params)
    {
        $this->iWantToSendHttpRequestTo($method, $uri);
        $this->requestHasParameters($params);
        $this->iSendIt();
    }


    /** @When /^I send http request "(\w+)" to "([\/\?\w\d]+)" with (text|html|json):$/ */
    public function iSendHttpRequestToWith($method, $uri, $contentType, PyStringNode $body)
    {
        $this->iWantToSendHttpRequestTo($method, $this->uriWithBase($uri));
        $this->requestHasBodyWithText($this->resolveContentType($contentType), $body);
        $this->iSendIt();
    }


    /** @Then the response status code should be :code */
    public function theResponseStatusCodeShouldBe($code)
    {
        Assertion::eq($this->getResponse()->getStatusCode(), $code);
    }


    /** @Then the response status phrase should be :phrase */
    public function theResponseStatusPhraseShouldBe($phrase)
    {
        Assertion::eq($this->getResponse()->getReasonPhrase(), $phrase);
    }


    /** @Then the response body should be equal to :contentType: */
    public function theResponseBodyShouldBeEqualToContent($contentType, PyStringNode $body)
    {
        $this->getResponse()->getBody()->rewind();
        Assertion::eq($this->getResponse()->getBody()->getContents(), $body->getRaw());
        Assertion::contains(
            $this->getResponse()->getHeaderLine('Content-Type'),
            $contentType
        );
    }


    /** @Then the response body should be contains to :contentType: */
    public function theResponseBodyShouldBeContainsToContent($contentType, PyStringNode $body)
    {
        $this->getResponse()->getBody()->rewind();
        Assertion::contains(
            $this->getResponse()->getBody()->getContents(),
            $body->getRaw()
        );
        Assertion::contains(
            $this->getResponse()->getHeaderLine('Content-Type'),
            $contentType
        );
    }


    /** @Then print the response */
    public function printTheResponse()
    {
        print $this->getResponse()->getBody()->getContents() . PHP_EOL;
    }


    /** @Transform :contentType */
    public function resolveContentType($contentType)
    {
        switch ($contentType) {
            case 'text':
                return 'text/plain';
            case 'html':
                return 'text/html';
            case 'json':
                return 'application/json';
        }
        return $contentType;
    }


    /** @Transform :uri */
    public function uriWithBase($uri)
    {
        return $this->baseUri.$uri;
    }


    public function toJsonBody($data)
    {
        return new PyStringNode([ json_encode($data, json_pretty_print_flags()) ], 0);
    }


    public function getResponse()
    {
        Assertion::isInstanceOf($this->response, ResponseInterface::class);
        return $this->response;
    }


    public function getClient()
    {
        Assertion::isInstanceOf($this->client, ClientInterface::class);
        return $this->client;
    }


    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
        return $this;
    }
}