<?php

use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\Response as PsrResponse;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use function GuzzleHttp\Psr7\stream_for;
use function App\get_parse_str;

final class Psr7Helper
{
    /**
     * @param RequestInterface $r
     * @return ServerRequest
     */
    public static function toPsrServerRequest(RequestInterface $r)
    {
        $server     = [];
        $parsedBody = strpos($r->getHeaderLine('Content-Type'), 'application/x-www-urlencode') > -1
            ? get_parse_str($r->getBody()->getContents())
            : [];

        if (strpos($r->getHeaderLine('Authorization'), 'Basic') === 0) {
            list($user, $password) = explode(':', base64_decode(explode(' ', $r->getHeaderLine('Authorization'))[1]));
            $server['AUTH_TYPE']     = 'Basic';
            $server['PHP_AUTH_USER'] = $user;
            $server['PHP_AUTH_PW']   = $password;
        }

        return (new ServerRequest(
            $r->getMethod(),
            $r->getUri(),
            $r->getHeaders(),
            $r->getBody(),
            $r->getProtocolVersion(),
            $server
        ))->withParsedBody($parsedBody);
    }


    /**
     * @param RequestInterface $r
     * @return Request
     */
    public static function toSymfonyRequest(RequestInterface $r)
    {
        $sr  = $r instanceof ServerRequestInterface ? $r : static::toPsrServerRequest($r);
        $sfr = Request::create(
            (string) $sr->getUri(),
            $sr->getMethod(),
            $sr->getParsedBody(),
            $sr->getCookieParams(),
            $sr->getUploadedFiles(),
            $sr->getServerParams(),
            (string) $sr->getBody()
        );
        $sfr->headers->add($sr->getHeaders());

        return $sfr;
    }


    /**
     * @param Response $sfr
     * @return PsrResponse
     */
    public static function toPsrResponse(Response $sfr)
    {
        $cookies = $sfr->headers->getCookies(ResponseHeaderBag::COOKIES_FLAT);
        $sfr->headers->removeCookie('');

        $response = (new PsrResponse(
            $sfr->getStatusCode(),
            $sfr->headers->all(),
            stream_for($sfr->getContent()),
            $sfr->getProtocolVersion()
        ));

        foreach ($cookies as $cookie) {
            $response = $response->withAddedHeader('Set-Cookie', $cookie);
        }

        return $response;
    }
}