<?php

use Assert\Assertion;
use Behat\Behat\Context\Context;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\RejectedPromise;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Debug\Debug;

class SymfonyContext implements Context
{
    /** @var string */
    private $env;
    /** @var bool */
    private $debug;


    public function __construct($env = 'test', $debug = null)
    {
        $this->symfonyEnvIs($env, $debug);
        require __DIR__.'/../../app/autoload.php';
        if ($this->debug) Debug::enable();
    }


    public function createHttpClient()
    {
        return new Client(['handler' => [$this, 'symfonyHttpHandler']]);
    }


    public function symfonyHttpHandler(RequestInterface $request, array $options) {
        try {
            $kernel  = new AppKernel($this->env, $this->debug);
            $kernel->loadClassCache();

            $request  = Psr7Helper::toSymfonyRequest($request);
            $response = $kernel->handle($request);
            $kernel->terminate($request, $response);

            return new FulfilledPromise(Psr7Helper::toPsrResponse($response));
        } catch (\Exception $e) {
            return new RejectedPromise($e);
        }
    }


    /** @Given symfony env is :env */
    public function symfonyEnvIs($env, $debug = null)
    {
        Assertion::choice($env = strtolower($env), ['prod', 'dev', 'test']);
        $this->env   = $env;
        $this->debug = !is_null($debug) ? $debug : $this->env !== 'prod';
    }
}