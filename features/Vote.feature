Feature: Vote

    Scenario: To vote success
        Given I want to send http request "POST" to "/api/votes/2/userVotes"
        And request has http basic auth:
            | user     | denis.pelikh |
            | password | matchclub    |
        And request has body with "json":
        """
            { "voted": 2 }
        """
        When I send it
        Then the response status code should be 201
        And  the response body should be contains to json:
        """
            "votedTeam": {
                "id": 2,
                "name": "ЦСКА"
            }
        """


    Scenario: Deny by timeout
        Given I want to send http request "POST" to "/api/votes/2/userVotes"
        And request has http basic auth:
            | user     | denis.pelikh |
            | password | matchclub    |
        And request has body with "json":
        """
            { "voted": 2 }
        """
        When I send it
        Then the response status code should be 403


    Scenario: Deny by money
        Given I want to send http request "POST" to "/api/votes/2/userVotes"
        And request has http basic auth:
            | user     | Tester w/o money |
            | password | matchclub        |
        And request has body with "json":
        """
            { "voted": 2 }
        """
        When I send it
        Then the response status code should be 403


    Scenario: To vote w/o money
        Given I want to send http request "POST" to "/api/votes/4/userVotes"
        And request has http basic auth:
            | user     | Tester w/o money |
            | password | matchclub        |
        And request has body with "json":
        """
            { "voted": 1 }
        """
        When I send it
        Then the response status code should be 201
        And the response body should be contains to json:
        """
            "votedPlayer": {
                "id": 1,
                "name": "Акинфеев",
                "team": {
                    "id": 2,
                    "name": "ЦСКА"
                }
            }
        """


    Scenario: To vote on invalid team
        Given I want to send http request "POST" to "/api/votes/2/userVotes"
        And request has http basic auth:
            | user     | maksim.kis   |
            | password | matchclub    |
        And request has body with "json":
        """
            { "voted": 1 }
        """
        When I send it
        Then the response status code should be 400


    Scenario: To vote on invalid player
        Given I want to send http request "POST" to "/api/votes/3/userVotes"
        And request has http basic auth:
            | user     | maksim.kis   |
            | password | matchclub    |
        And request has body with "json":
        """
            { "voted": 1 }
        """
        When I send it
        Then the response status code should be 400


    Scenario: Get vote w/o UserVotes as user
        Given I want to send http request "GET" to "/api/votes/1"
        And request has http basic auth:
            | user     | denis.pelikh   |
            | password | matchclub    |
        When I send it
        Then the response status code should be 200
        And the response body should be contains to json:
        """
            "userVotes": [],
            "timeout": 0,
            "countUserVotes": 0,
            "countUserVotesByTeams": {
                "Зенит": 0,
                "ЦСКА": 0
            }
        """

    Scenario: Get vote on team with UserVotes as user
        Given I want to send http request "GET" to "/api/votes/2"
        And request has http basic auth:
            | user     | denis.pelikh   |
            | password | matchclub    |
        When I send it
        Then the response status code should be 200
        And the response body should be contains to json:
        """
            "countUserVotes": 1,
            "countUserVotesByTeams": {
                "Спартак": 0,
                "ЦСКА": 1
            }
        """

    Scenario: Get vote on player with UserVotes as user
        Given I want to send http request "GET" to "/api/votes/3"
        And request has http basic auth:
            | user     | denis.pelikh   |
            | password | matchclub    |
        When I send it
        Then the response status code should be 200
        """
            "votedPlayer": {
                "id": 2,
                "name": "Лодыгин",
                "team": {
                    "id": 1,
                    "name": "Зенит"
                }
            },
            "user": {
                "id": 3,
                "name": "denis.pelikh",
                "money": 4
            }
        """

    Scenario: List votes as anonymous
        When I send http request "GET" to "/api/votes"
        Then the response status code should be 200
        And the response body should be equal to json:
        """
        [
            {
                "id": 3,
                "type": "on_player",
                "interval": 60,
                "cost": 1,
                "game": {
                    "id": 3,
                    "startDate": "2017-02-06 23:45:00",
                    "endDate": "2017-02-07 01:21:00",
                    "homeTeam": {
                        "id": 3,
                        "name": "Спартак"
                    },
                    "awayTeam": {
                        "id": 1,
                        "name": "Зенит"
                    },
                    "score": {
                        "homeTeam": 2,
                        "awayTeam": 3
                    }
                }
            },
            {
                "id": 2,
                "type": "on_team",
                "interval": 60,
                "cost": 1,
                "game": {
                    "id": 2,
                    "startDate": "2017-01-01 01:30:00",
                    "endDate": "2017-01-01 03:14:00",
                    "homeTeam": {
                        "id": 3,
                        "name": "Спартак"
                    },
                    "awayTeam": {
                        "id": 2,
                        "name": "ЦСКА"
                    },
                    "score": {
                        "homeTeam": 1,
                        "awayTeam": 0
                    }
                }
            },
            {
                "id": 4,
                "type": "on_player",
                "interval": 60,
                "cost": 0,
                "game": {
                    "id": 1,
                    "startDate": "2016-12-10 21:00:00",
                    "endDate": "2016-12-10 22:40:00",
                    "homeTeam": {
                        "id": 1,
                        "name": "Зенит"
                    },
                    "awayTeam": {
                        "id": 2,
                        "name": "ЦСКА"
                    },
                    "score": {
                        "homeTeam": 0,
                        "awayTeam": 2
                    }
                }
            },
            {
                "id": 1,
                "type": "on_team",
                "interval": 60,
                "cost": 0,
                "game": {
                    "id": 1,
                    "startDate": "2016-12-10 21:00:00",
                    "endDate": "2016-12-10 22:40:00",
                    "homeTeam": {
                        "id": 1,
                        "name": "Зенит"
                    },
                    "awayTeam": {
                        "id": 2,
                        "name": "ЦСКА"
                    },
                    "score": {
                        "homeTeam": 0,
                        "awayTeam": 2
                    }
                }
            }
        ]
        """